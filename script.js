$('#insere-jogo').on('submit', function (event) {
    event.preventDefault();
    $.ajax({
        url: 'api.php?acao=cadastrar',
        method: 'POST',
        data: $(this).serialize()
    })
        .done(function (data) {
            console.log(data);
            if (data.erro) {
                alert(data.erro)
            } else {
                var tabela = $('table tbody');
                var linha = $('<tr>')
                linha.append('<td><img src="' + data.imagem + '"></td>');
                linha.append('<td>' + data.titulo + '</td>');
                linha.append('<td>' + data.genero + '</td> ');
                linha.append('<td>' + data.ano + '</td> ');
                linha.append('<td>' + data.empresa + '</td>');
                linha.append('<td>' + data.plataformas + '</td>');
                linha.append('<td><img class="trash" src="trash.png" alt="trash"></td>');
                tabela.append(linha);
            }
        })
        .fail(function () {
            alert("Falha  ao cadastrar jogo!");
        })
})

$('table').on('click', '.trash', function () {
    var trash = $(this);
    var id = trash.data('id');
    $.ajax({
        url: 'api.php?acao=deletar&jogo_id=' + id,
        method: 'GET'
    })
    .done(function(data){
        if(data.erro){
            alert(data.erro);
        }else{
            trash.closest("tr").remove();
        }
    })
})