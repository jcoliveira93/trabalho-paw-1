<?php 
error_reporting(E_ALL);

session_start();
include_once 'bd.php';
if(!$_SESSION['UsuarioId']){
    header("Location: login.php");
}

$db = new Db();

$sql = "SELECT
    j.id id,
    titulo,
    imagem,
    ano,
    g.genero genero,
    e.nome empresa,
    GROUP_CONCAT(p.plataforma) plataformas
FROM
    Jogo j
INNER JOIN Genero g ON
    j.GeneroId = g.id
INNER JOIN Empresa e ON
    j.EmpresaId = e.id
INNER JOIN Jogo_Plataforma jp ON
    jp.JogoId = j.id
INNER JOIN Plataforma p ON
    p.id = jp.PlataformaId
WHERE
    j.UsuarioId = '$_SESSION[UsuarioId]'
GROUP BY
    j.id";

$result = $db->query($sql);
$result_plataformas = $db->query("SELECT * FROM Plataforma");
$result_generos = $db->query("SELECT * FROM Genero");
$result_empresas = $db->query("SELECT * FROM Empresa");
?>

<?php include 'header.php';?>
<table>
    <thead>
        <tr>
            <th></th>
            <th>Titulo</th>
            <th>Genero</th> 
            <th>Ano</th>
            <th>Empresa</th>
            <th>Plataformas</th>
            <th>Deletar</th>
        </tr>
    </thead>
    <tbody>

<?php if($result->num_rows > 0) { ?>

    <?php while($jogo = $result->fetch_object()){ ?>
        <tr>
            <td><img src="<?= $jogo->imagem ?>" alt="<?= $jogo->titulo ?>"></td> 
            <td><?= $jogo->titulo ?></td>
            <td><?= $jogo->genero ?></td> 
            <td><?= $jogo->ano ?></td> 
            <td><?= $jogo->empresa ?></td>
            <td><?= $jogo->plataformas ?></td>
            <td><img class="trash" src="trash.png" alt="trash" data-id="<?= $jogo->id ?>"></td>
        </tr>
    <?php } ?>

<?php } else{ ?>
    <div>
        Nenhum jogo cadastrado!
    </div>
<?php } ?>
    </tbody>
</table>
<form method="POST" id="insere-jogo">
<div>
    <label for="titulo">Titulo</label>
    <input type="text" name="titulo" required>
</div>
<div>
    <label for="ano">Ano</label>
    <input type="text" name="ano" required>
</div>
<div>
    <label for="imagem">Url da Imagem</label>
    <input type="text" name="imagem" required>
</div>
<div>
    <label for="GeneroId">Gênero</label>
    <select name="GeneroId">
    <?php
        while($genero = $result_generos->fetch_object()){
            echo "<option value='$genero->id'>$genero->genero</option>";
        }
    ?>
    </select>
</div>
<div>
    <label for="EmpresaId">Empresa</label>
    <select name="EmpresaId">
    <?php
        while($empresa = $result_empresas->fetch_object()){
            echo "<option value='$empresa->id'>$empresa->nome</option>";
        }
    ?>
    </select>
</div>
<div>
    <label for="plataformas">Plataformas</label>
    <?php
        while($plataforma = $result_plataformas->fetch_object()){
            echo "<label><input name='plataformas[]' value='$plataforma->id' type='checkbox'>$plataforma->plataforma</label>";
        }
    ?>
</div>
<button type="submit">Salvar</button>
</form>

<script src='script.js'></script>

<?php include 'footer.php';?>
