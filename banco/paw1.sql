-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 10, 2017 at 11:28 PM
-- Server version: 10.0.31-MariaDB-0ubuntu0.16.04.2
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paw1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Empresa`
--

CREATE TABLE `Empresa` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Empresa`
--

INSERT INTO `Empresa` (`id`, `nome`) VALUES
(1, 'Blizzard'),
(2, 'Valve'),
(3, 'Rockstar'),
(4, 'Bethesda');

-- --------------------------------------------------------

--
-- Table structure for table `Genero`
--

CREATE TABLE `Genero` (
  `id` int(11) NOT NULL,
  `genero` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Genero`
--

INSERT INTO `Genero` (`id`, `genero`) VALUES
(1, 'Moba');

-- --------------------------------------------------------

--
-- Table structure for table `Jogo`
--

CREATE TABLE `Jogo` (
  `id` int(11) NOT NULL,
  `titulo` varchar(200) NOT NULL,
  `ano` varchar(4) NOT NULL,
  `GeneroId` int(11) NOT NULL,
  `imagem` varchar(255) NOT NULL,
  `UsuarioId` int(11) NOT NULL,
  `EmpresaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Jogo_Plataforma`
--

CREATE TABLE `Jogo_Plataforma` (
  `JogoId` int(11) NOT NULL,
  `PlataformaId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Plataforma`
--

CREATE TABLE `Plataforma` (
  `id` int(11) NOT NULL,
  `plataforma` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Plataforma`
--

INSERT INTO `Plataforma` (`id`, `plataforma`) VALUES
(1, 'PC'),
(2, 'PS4'),
(3, 'Xbox One'),
(4, 'MAC');

-- --------------------------------------------------------

--
-- Table structure for table `Usuario`
--

CREATE TABLE `Usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `login` varchar(100) NOT NULL,
  `senha` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Usuario`
--

INSERT INTO `Usuario` (`id`, `nome`, `login`, `senha`) VALUES
(5, 'Daniel da Silva Xavier Oliveira', 'daniel', '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9'),
(6, 'Jivago Medeiros', 'jivago', '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Genero`
--
ALTER TABLE `Genero`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Jogo`
--
ALTER TABLE `Jogo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_UsuarioId` (`UsuarioId`),
  ADD KEY `fk_EmpresaId` (`EmpresaId`);

--
-- Indexes for table `Plataforma`
--
ALTER TABLE `Plataforma`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Empresa`
--
ALTER TABLE `Empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Genero`
--
ALTER TABLE `Genero`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Jogo`
--
ALTER TABLE `Jogo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `Plataforma`
--
ALTER TABLE `Plataforma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Jogo`
--
ALTER TABLE `Jogo`
  ADD CONSTRAINT `fk_EmpresaId` FOREIGN KEY (`EmpresaId`) REFERENCES `Empresa` (`id`),
  ADD CONSTRAINT `fk_UsuarioId` FOREIGN KEY (`UsuarioId`) REFERENCES `Usuario` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
