<?php
session_start();
header('Content-type: application/json');
include_once 'bd.php';
    $db = new Db();
if(!$_SESSION['UsuarioId']){
    echo '{ "erro": "Não existe sessão!"}';
    exit();
}
if(isset($_GET['acao']) && $_GET['acao'] == 'cadastrar'){

    $erros = array();
    if(!isset($_POST['titulo'])){
        $erros[] = "O campo titulo é obrigatório!";
    }
    if(!isset($_POST['ano'])){
        $erros[] = "O campo ano é obrigatório!";
    }
    if(!isset($_POST['imagem'])){
        $erros[] = "O campo url da imagem é obrigatório!";
    }
    if(!isset($_POST['GeneroId'])){
        $erros[] = "O campo genero é obrigatório!";
    }
    if(!isset($_POST['EmpresaId'])){
        $erros[] = "O campo empresa é obrigatório!";
    }
    if(!isset($_POST['plataformas'])){
        $erros[] = "Escolha pelo menos uma plataforma";
    }


    if(count($erros) > 0){
        echo '{"erros": "'.json_encode($erros).'"}';
        exit();
    }


    $sql_jogo = "INSERT INTO
        `Jogo`(
            `titulo`,
            `ano`,
            `GeneroId`,
            `imagem`,
            `UsuarioId`,
            `EmpresaId`
        )
        VALUES(
        '$_POST[titulo]',
        $_POST[ano],
        $_POST[GeneroId],
        '$_POST[imagem]',
        $_SESSION[UsuarioId],
        $_POST[EmpresaId]
        )";

    $result = $db->query($sql_jogo);

    $jogo_id = $db->insert_id();

    $plataformas = array();

    for($i = 0; $i < count($_POST['plataformas']); $i++){
        $plataforma_id = $_POST['plataformas'][$i];
        $plataformas[] = "$jogo_id, $plataforma_id";
    }
    $sql_jogo_plataforma = "INSERT INTO
        `Jogo_Plataforma`(
            `JogoId`,
            `PlataformaId`
        )
        VALUES
        (".implode("),(",$plataformas).")";

    $db->query($sql_jogo_plataforma);

    $novoJogo = "SELECT
        j.id id,
        titulo,
        imagem,
        ano,
        g.genero genero,
        e.nome empresa,
        GROUP_CONCAT(p.plataforma) plataformas
    FROM
        Jogo j
    INNER JOIN Genero g ON
        j.GeneroId = g.id
    INNER JOIN Empresa e ON
        j.EmpresaId = e.id
    INNER JOIN Jogo_Plataforma jp ON
        jp.JogoId = j.id
    INNER JOIN Plataforma p ON
        p.id = jp.PlataformaId
    WHERE
        j.id = $jogo_id
    GROUP BY
        j.titulo";

    $jogoQuery = $db->query($novoJogo);
    if($jogoQuery->num_rows > 0){
        $jogo = $jogoQuery->fetch_object();
        echo json_encode($jogo);
    }else{
        echo '{"erro":"Erro ao inserir jogo!"}';
    }
}

if(isset($_GET['acao']) && $_GET['acao'] == 'deletar'){
    $sql_deletar = "DELETE FROM Jogo WHERE id = $_GET[jogo_id] AND UsuarioId = $_SESSION[UsuarioId]";
    $teste = $db->query($sql_deletar);
    echo '{"message":"Jogo deletado"}';
}
?>